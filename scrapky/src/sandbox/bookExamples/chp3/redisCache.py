import json

from datetime import timedelta
from redis import StrictRedis

class RedisCache:
	def __init__(self,client=None,expires=timedelta(days=30),encoding='utf-8'):
		# if a client object is not passed the try
		# connect tio redis at the default localhost port
		self.client = StrictRedis(host='localhost',port=6379,db=0) if client is None else client
		self.expires = expires
		self.encoding = encoding

	def __getitem__(self, url):
		'''Load value from Redis for the given URL'''
		record = self.client.get(url)
		if record:
			return json.loads(record.decode(self.encoding))
		else:
			raise KeyError(url+'does not exists')

	def __setitem__(self,url,result):
		'''Save value in Redis for given URL '''
		data = bytes(json.dumps(result),self.encoding)
		self.client.setex(url,self.expires, data)
